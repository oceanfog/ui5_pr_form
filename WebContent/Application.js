var v_hello = "hello";

//Create an instance of the table control
var oTable = new sap.ui.table.Table({
	title : "",
	rowHeight : 20,
	visibleRowCount : 3,
	firstVisibleRow : 3,
	selectionMode : sap.ui.table.SelectionMode.Single,

});

//Define the columns and the control templates to be used


oTable.addColumn(new sap.ui.table.Column({
	label : new sap.ui.commons.Label({
		text : "Last Name"
	}),
	template : new sap.ui.commons.TextView().bindProperty("text", "lastName"),
	sortProperty : "lastName",
	filterProperty : "lastName",
	width : "200px"
}));

oTable.addColumn(new sap.ui.table.Column({
	label : new sap.ui.commons.Label({
		text : "First Name"
	}),
	template : new sap.ui.commons.TextField().bindProperty("value", "name"),
	sortProperty : "name",
	filterProperty : "name",
	width : "100px"
}));
oTable.addColumn(new sap.ui.table.Column({
	label : new sap.ui.commons.Label({
		text : "Checked"
	}),
	template : new sap.ui.commons.CheckBox().bindProperty("checked", "checked"),
	sortProperty : "checked",
	filterProperty : "checked",
	width : "75px",
	hAlign : "Center"
}));





oButton_01 = new sap.ui.commons.Button({ text:'bu_02'});
oLabel_01 = new sap.ui.commons.Label({ text:'hello_01', layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}), // sap.ui.core.Label
oLabel_02 = new sap.ui.commons.Label({ text:'nello_02', layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}), // sap.ui.core.Label
oLabel_05 = new sap.ui.commons.Label({ text:'nello_02', layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}), // sap.ui.core.Label

oTextField_01 = new sap.ui.commons.TextField({ value:""	});
oTextField_02 = new sap.ui.commons.TextField({	});
oTextField_03 = new sap.ui.commons.TextField({	});
oTextField_05 = new sap.ui.commons.TextField({	});



oRadioButtonGroup =	new sap.ui.commons.RadioButtonGroup({
	columns : 3, // int
	items : [ 
		new sap.ui.core.Item({	text : "G/L 계정"	}),
		new sap.ui.core.Item({	text : "구매처 계정",	}),  
		new sap.ui.core.Item({	text : "고객 계정",	}) 
	], 
	
	select : [ function(oEvent) {
		var control = oEvent.getSource();
	}, this ]
});

oDropDownBox = new sap.ui.commons.DropdownBox({
	layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}),
	id : "", // sap.ui.core.ID
	items : [
		new sap.ui.core.ListItem("", {text:"-----"}),
		new sap.ui.core.ListItem("", {text:"nello"}),
	], // sap.ui.core.ListItem
});

oDropDownBox_02 = new sap.ui.commons.DropdownBox({
	layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"}),
	id : "", // sap.ui.core.ID
	items : [
		new sap.ui.core.ListItem("", {text:"-----"}),
		new sap.ui.core.ListItem("", {text:"nello"}),
	], // sap.ui.core.ListItem
});


oDatePicker = new sap.ui.commons.DatePicker({yyyymmdd: "20140221",
    layoutData: new sap.ui.core.VariantLayoutData({
        multipleLayoutData: [new sap.ui.layout.ResponsiveFlowLayoutData({weight: 2}),
                             new sap.ui.layout.form.GridElementData({hCells: "2"}),
                             ]
        })
});

oDatePicker_02 = new sap.ui.commons.DatePicker({yyyymmdd: "20140221",
    layoutData: new sap.ui.core.VariantLayoutData({
        multipleLayoutData: [new sap.ui.layout.ResponsiveFlowLayoutData({weight: 2}),
                             new sap.ui.layout.form.GridElementData({hCells: "2"}),
                             ]
        })
});


oFormElement = new sap.ui.commons.form.FormElement({
	fields : [ 
		new sap.ui.commons.Label({ text:'요청번호', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}) }),
		new sap.ui.commons.TextField({ value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"})}),
		new sap.ui.commons.Label({ text:'전표번호', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"}) }),
		new sap.ui.commons.TextField({ value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"})}),
	]
});

oFormElement_02 = new sap.ui.commons.form.FormElement({
	fields : [ 
		new sap.ui.commons.Label({ text:'전기일자', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		oDatePicker,
		new sap.ui.commons.Label({ text:'', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"})}),
		new sap.ui.commons.Label({ text:'증빙일자', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		oDatePicker_02,
		new sap.ui.commons.Label({ text:'', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"})}),
	]
});

oFormElement_03 = new sap.ui.commons.form.FormElement({
	fields : [ 
		new sap.ui.commons.Label({text:'통화', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		new sap.ui.commons.TextField({ id:"tf_01", value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "4"})}),
	]
// sap.ui.core.Control
});

oFormElement_04 = new sap.ui.commons.form.FormElement({
	fields : [ 
		new sap.ui.commons.Label({ text:'적요', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		new sap.ui.commons.TextField({ value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "10"})}),
	]
// sap.ui.core.Control
});
oFormElement_05 = new sap.ui.commons.form.FormElement({
	fields : [ 
		new sap.ui.commons.Label({ text:'구분', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		oRadioButtonGroup
		

	]
// sap.ui.core.Control
});

oFormElement_06 = new sap.ui.layout.form.FormElement({
	fields : [ 
		new sap.ui.commons.Label({ text:'계정', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		oDropDownBox,
		oDropDownBox_02,
	]
// sap.ui.core.Control
});

oFormElement_07 = new sap.ui.layout.form.FormElement({
	fields : [ 
		new sap.ui.commons.Label({ text:'코스트 센터', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		new sap.ui.commons.TextField({ value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"}) }),
		new sap.ui.commons.TextField({ value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}) }),
		new sap.ui.commons.Label({ text:'예산관리 센터', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		new sap.ui.commons.TextField({ value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"}) }),
		new sap.ui.commons.TextField({ value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}) }),
	]
// sap.ui.core.Control
});

oFormElement_08 = new sap.ui.layout.form.FormElement({
	fields : [ 
		new sap.ui.commons.Label({ text:'지정', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		new sap.ui.commons.TextField({ value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "10"}) }),
		new sap.ui.commons.Button({ text:'기타입력사항', layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
	]
// sap.ui.core.Control
});


oFormElement_09 = new sap.ui.layout.form.FormElement({
	fields : [ 
		new sap.ui.commons.Label({ text:'공급가액', textAlign : sap.ui.core.TextAlign.Center, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
		new sap.ui.commons.TextField({ value:"", layoutData: new sap.ui.layout.form.GridElementData({hCells: "3"})}),
		new sap.ui.commons.Label({ text:'KRW', textAlign : sap.ui.core.TextAlign.Orign, layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
	]
// sap.ui.core.Control
});


oFormElement_10 = new sap.ui.layout.form.FormElement({
	fields : [ 
		new sap.ui.commons.Button({ text:'추가', layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}), press:[function(oEvent) { alert("hello") }, this] }),
		new sap.ui.commons.Label({ text:'추가버튼을 클릭하시면 입력하신 내용이 추가됩니다.', layoutData: new sap.ui.layout.form.GridElementData({hCells: "10"}) }),
		
	]
// sap.ui.core.Control
});




oFormContainer = new sap.ui.commons.form.FormContainer({
	title : new sap.ui.core.Title({
		text : "기본정보", // string
	}),
	formElements:[
		oFormElement,
		oFormElement_02,
		oFormElement_03,
		oFormElement_04,
	],
});

oFormContainer_02 = new sap.ui.commons.form.FormContainer({
	title : new sap.ui.core.Title({
		text : "세부사항", // string
	}),
	formElements:[
		
		oFormElement_05,
		oFormElement_06,
		oFormElement_07,
		oFormElement_08,
		oFormElement_09,
		oFormElement_10,
		
	],
});


oGridLayout = new sap.ui.layout.form.GridLayout("L1");

oFormLayout = new sap.ui.layout.form.FormLayout({
});

oResponsiveLayout = new sap.ui.layout.form.ResponsiveLayout({ });

oResponsiveGridLayout = new sap.ui.layout.form.ResponsiveGridLayout({
	labelSpanL : 4, // int, since 1.16.3
	labelSpanM : 2, // int, since 1.16.3
	labelSpanS : 12, // int, since 1.16.3
	emptySpanL : 0, // int, since 1.16.3
	emptySpanM : 0, // int, since 1.16.3
	emptySpanS : 0, // int, since 1.16.3
	columnsL : 2, // int, since 1.16.3
	columnsM : 1, // int, since 1.16.3
	breakpointL : 1024, // int, since 1.16.3
	breakpointM : 600, // int, since 1.16.3
// sap.ui.core.CustomData
});




oFormContainer_03 = new sap.ui.commons.form.FormContainer({
	title : new sap.ui.core.Title({
		text : "form container_03", // string
	}),
	formElements:[
		new sap.ui.commons.form.FormElement({
			label : new sap.ui.commons.Label({ text:'nello_'}),
			fields : [ 
				new sap.ui.commons.Button({ text:'gridLayout', press:[function(oEvent) { oForm.setLayout( oGridLayout ); }, this]  }),
				new sap.ui.commons.Button({ text:'formLayout', press:[function(oEvent) { oForm.setLayout( oFormLayout ) }, this]  }),
				new sap.ui.commons.Button({ text:'responsiveLayout', press:[function(oEvent) { oForm.setLayout( oResponsiveLayout ) }, this] }),
				new sap.ui.commons.Button({ text:'responsiveGridLayout', press:[function(oEvent) { oForm.setLayout( oResponsiveGridLayout ) }, this] }),
			]
		// sap.ui.core.Control
		}),
	],
});

oFormContainer_04 = new sap.ui.commons.form.FormContainer({
	title : new sap.ui.core.Title({
		text : "form container_04", // string
	}),
	formElements:[
		new sap.ui.commons.form.FormElement({
			label : new sap.ui.commons.Label({ text:'nello_'}),
			fields : [ 
				new sap.ui.commons.Button({ text:'submit', layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}), press:[function(oEvent) { alert( jQuery("#tf_01").val() ) }, this]  }),
			]
		// sap.ui.core.Control
		}),
	],
});


oForm = new sap.ui.commons.form.Form({
	layout:oGridLayout,
	title:"oFrom",
	width:"80%",
	formContainers : [ 
		oFormContainer,
		oFormContainer_02,
		// oFormContainer_03,
		// oFormContainer_04,
		
	], // sap.ui.layout.form.FormContainer
});