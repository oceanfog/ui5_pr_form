sap.ui.jsview("ui5_pr_form.initForm", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf ui5_pr_form.initForm
	*/ 
	getControllerName : function() {
		return "ui5_pr_form.initForm";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf ui5_pr_form.initForm
	*/ 
	createContent : function(oController) {

		new sap.ui.table.Table({
			id : "id", // sap.ui.core.ID
			width : "auto", // sap.ui.core.CSSSize
			rowHeight : undefined, // int
			columnHeaderHeight : undefined, // int
			columnHeaderVisible : true, // boolean
			visibleRowCount : 10, // int
			firstVisibleRow : 0, // int
			selectionMode : sap.ui.table.SelectionMode.Multi, // sap.ui.table.SelectionMode
			selectionBehavior : sap.ui.table.SelectionBehavior.RowSelector, // sap.ui.table.SelectionBehavior
			selectedIndex : -1, // int
			allowColumnReordering : true, // boolean
			editable : true, // boolean
			visible : true, // boolean
			navigationMode : sap.ui.table.NavigationMode.Scrollbar, // sap.ui.table.NavigationMode
			threshold : 100, // int
			enableColumnReordering : true, // boolean
			enableGrouping : false, // boolean
			showColumnVisibilityMenu : false, // boolean
			showNoData : true, // boolean
			visibleRowCountMode : sap.ui.table.VisibleRowCountMode.Fixed, // sap.ui.table.VisibleRowCountMode, since 1.9.2
			fixedColumnCount : 0, // int
			fixedRowCount : 0, // int
			minAutoRowCount : 5, // int
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "id1", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			title : undefined, // sap.ui.core.Control
			footer : undefined, // sap.ui.core.Control
			toolbar : new sap.ui.commons.Toolbar({
				id : "id2", // sap.ui.core.ID
				visible : true, // boolean
				width : "auto", // sap.ui.core.CSSSize
				design : sap.ui.commons.ToolbarDesign.Flat, // sap.ui.commons.ToolbarDesign
				standalone : true, // boolean
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : [], // sap.ui.core.CustomData
				items : [], // sap.ui.commons.ToolbarItem
				rightItems : []
			// sap.ui.commons.ToolbarItem
			}), // sap.ui.commons.Toolbar
			extension : [], // sap.ui.core.Control
			columns : [ new sap.ui.table.Column({
				id : "id3", // sap.ui.core.ID
				width : undefined, // sap.ui.core.CSSSize
				flexible : true, // boolean
				resizable : true, // boolean
				hAlign : sap.ui.commons.layout.HAlign.Begin, // sap.ui.commons.layout.HAlign
				sorted : false, // boolean
				sortOrder : sap.ui.table.SortOrder.Ascending, // sap.ui.table.SortOrder
				sortProperty : undefined, // string
				filtered : false, // boolean
				filterProperty : undefined, // string
				filterValue : undefined, // string
				filterOperator : undefined, // string
				grouped : false, // boolean
				visible : true, // boolean
				filterType : undefined, // any, since 1.9.2
				name : undefined, // string, since 1.11.1
				showFilterMenuEntry : true, // boolean, since 1.13.0
				showSortMenuEntry : true, // boolean, since 1.13.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : [], // sap.ui.core.CustomData
				label : undefined, // sap.ui.core.Control
				multiLabels : [], // sap.ui.core.Control, since 1.13.1
				template : undefined, // sap.ui.core.Control
				menu : new sap.ui.commons.Menu({
					id : "id4", // sap.ui.core.ID
					enabled : true, // boolean
					ariaDescription : undefined, // string
					tooltip : undefined, // sap.ui.core.TooltipBase
					customData : [], // sap.ui.core.CustomData
					items : [ new sap.ui.commons.MenuItemBase({
						id : "id5", // sap.ui.core.ID
						enabled : true, // boolean
						visible : true, // boolean
						startsSection : false, // boolean
						tooltip : undefined, // sap.ui.core.TooltipBase
						customData : [], // sap.ui.core.CustomData
						submenu : undefined, // sap.ui.commons.Menu
						select : [ function(oEvent) {
							var control = oEvent.getSource();
						}, this ]
					}) ], // sap.ui.commons.MenuItemBase
					itemSelect : [ function(oEvent) {
						var control = oEvent.getSource();
					}, this ]
				})
			// sap.ui.commons.Menu
			}) ], // sap.ui.table.Column
			rows : [ new sap.ui.table.Row({
				id : "id6", // sap.ui.core.ID
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : [], // sap.ui.core.CustomData
				cells : []
			// sap.ui.core.Control
			}) ], // sap.ui.table.Row
			noData : undefined, // sap.ui.core.Control
			groupBy : undefined, // sap.ui.table.Column
			rowSelectionChange : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			columnSelect : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			columnResize : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			columnMove : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			sort : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			filter : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			group : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			columnVisibility : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ]
		})
		
		new sap.ca.ui.DatePicker({
			id : "id", // sap.ui.core.ID
			value : undefined, // string
			width : undefined, // sap.ui.core.CSSSize
			enabled : true, // boolean
			visible : true, // boolean
			valueState : sap.ui.core.ValueState.None, // sap.ui.core.ValueState
			name : undefined, // string
			placeholder : undefined, // string
			editable : true, // boolean, since 1.12.0
			firstDayOffset : 0, // int
			dateValue : undefined, // string
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "id1", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			change : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ]
		})
		
		new sap.ui.commons.Button({
			text : '', // string
			press : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ]
		})
		
		new sap.ui.layout.form.ResponsiveLayout({
		});
		
		
		new sap.ui.layout.form.ResponsiveGridLayout({
			labelSpanL : 4, // int, since 1.16.3
			labelSpanM : 2, // int, since 1.16.3
			labelSpanS : 12, // int, since 1.16.3
			emptySpanL : 0, // int, since 1.16.3
			emptySpanM : 0, // int, since 1.16.3
			emptySpanS : 0, // int, since 1.16.3
			columnsL : 2, // int, since 1.16.3
			columnsM : 1, // int, since 1.16.3
			breakpointL : 1024, // int, since 1.16.3
			breakpointM : 600, // int, since 1.16.3
		// sap.ui.core.CustomData
		})
		
		new sap.ui.layout.form.FormLayout({
		});
		
		new sap.ui.layout.form.GridLayout({
			id : "id", // sap.ui.core.ID
			singleColumn : false, // boolean
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "id1", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ]
		// sap.ui.core.CustomData
		})
		
		new sap.ui.commons.form.FormElement({
			label : undefined, // sap.ui.core.Label
			fields : []
		// sap.ui.core.Control
		})
		
		
		new sap.ui.commons.form.FormContainer({
			title : new sap.ui.core.Title({
				text : "hello", // string
			})
		})
		
		
		oForm = new sap.ui.commons.form.Form({
			id : "id", // sap.ui.core.ID
			width : undefined, // sap.ui.core.CSSSize
			visible : true, // boolean
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "id1", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			formContainers : [ new sap.ui.layout.form.FormContainer({
				id : "id2", // sap.ui.core.ID
				expanded : true, // boolean
				expandable : false, // boolean
				visible : true, // boolean
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : [], // sap.ui.core.CustomData
				formElements : [ new sap.ui.layout.form.FormElement({
					id : "id3", // sap.ui.core.ID
					visible : true, // boolean
					tooltip : undefined, // sap.ui.core.TooltipBase
					customData : [], // sap.ui.core.CustomData
					label : undefined, // sap.ui.core.Label
					fields : []
				// sap.ui.core.Control
				}) ], // sap.ui.layout.form.FormElement
				title : new sap.ui.core.Title({
					id : "id4", // sap.ui.core.ID
					text : undefined, // string
					icon : undefined, // sap.ui.core.URI
					level : sap.ui.core.TitleLevel.Auto, // sap.ui.core.TitleLevel
					emphasized : false, // boolean
					tooltip : undefined, // sap.ui.core.TooltipBase
					customData : []
				// sap.ui.core.CustomData
				})
			// sap.ui.core.Title
			}) ], // sap.ui.layout.form.FormContainer
			title : undefined, // sap.ui.core.Title
			layout : new sap.ui.layout.form.FormLayout({
				id : "id5", // sap.ui.core.ID
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			})
		// sap.ui.layout.form.FormLayout
		})
	}

	
	
});
